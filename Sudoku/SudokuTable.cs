﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sudoku
{
    internal class SudokuTable
    {
        private enum BlockType { String, Column, Block };

        private TableElement[,] table = new TableElement[,] 
        {
            { new TableElement(), new TableElement(), new TableElement(), new TableElement(), new TableElement(), new TableElement(), new TableElement(), new TableElement(), new TableElement() },
            { new TableElement(), new TableElement(), new TableElement(), new TableElement(), new TableElement(), new TableElement(), new TableElement(), new TableElement(), new TableElement() },
            { new TableElement(), new TableElement(), new TableElement(), new TableElement(), new TableElement(), new TableElement(), new TableElement(), new TableElement(), new TableElement() },
            { new TableElement(), new TableElement(), new TableElement(), new TableElement(), new TableElement(), new TableElement(), new TableElement(), new TableElement(), new TableElement() },
            { new TableElement(), new TableElement(), new TableElement(), new TableElement(), new TableElement(), new TableElement(), new TableElement(), new TableElement(), new TableElement() },
            { new TableElement(), new TableElement(), new TableElement(), new TableElement(), new TableElement(), new TableElement(), new TableElement(), new TableElement(), new TableElement() },
            { new TableElement(), new TableElement(), new TableElement(), new TableElement(), new TableElement(), new TableElement(), new TableElement(), new TableElement(), new TableElement() },
            { new TableElement(), new TableElement(), new TableElement(), new TableElement(), new TableElement(), new TableElement(), new TableElement(), new TableElement(), new TableElement() },
            { new TableElement(), new TableElement(), new TableElement(), new TableElement(), new TableElement(), new TableElement(), new TableElement(), new TableElement(), new TableElement() }
        };

        public void Load(string input)
        {
            if (input.Length < 81) return;
            for (int i = 0; i < 9; i++)
                for (int j = 0; j < 9; j++)
                {
                    if (int.TryParse(input[9 * i + j].ToString(), out int val)) SetValue(i, j, val);
                }
        }

        private void SetValue(int x, int y, int value)
        {
            table[x, y].Set(value);
            UnsetBlock(BlockType.String, x, value);
            UnsetBlock(BlockType.Column, y, value);
            UnsetBlock(BlockType.Block, 3*(x/3)+y/3, value);
        }

        private void UnsetBlock(BlockType type, int x, int value)
        {
            switch (type)
            {
                case BlockType.String:
                    for (int i = 0; i < 9; i++) if (table[x, i].HasVal(value)) table[x, i].UnSet(value);
                    break;
                case BlockType.Column:
                    for (int i = 0; i < 9; i++) if (table[i, x].HasVal(value)) table[i, x].UnSet(value);
                    break;
                case BlockType.Block:
                    for (int i = 0; i < 9; i++) if (table[3*(x/3)+i/3, 3*(x%3)+i%3].HasVal(value)) table[3 * (x / 3) + i / 3, 3 * (x % 3) + i % 3].UnSet(value);
                    break;
            }
        }

        public override string ToString()
        {
            string res = "-------------------------\n";
            for (int i=0; i<9; i++)
            {
                res += "| ";
                for (int j=0; j<9; j++)
                {
                    int val = table[i, j].Value();
                    res += (val == 0 ? "." : val.ToString()) + " ";
                    if ((j + 1) % 3 == 0) res += "| ";
                }
                res += "\n";
                if ((i + 1) % 3 == 0) res += "-------------------------\n"; 
            }
            return res;
        }

        public bool IsSolved()
        {
            return Enumerable.Range(0, 9).Select(x => Enumerable.Range(0, 9).Select(y => table[x, y].IsSet()).Where(n => !n)).Where(n => n.Count() > 0).Count() == 0;
        }

        public void Solve()
        {
            bool Checks = true;
            while (Checks)
            {
                Checks = false;
                Checks |= CheckSingle();
                Checks |= LastHero();
            }
        }

        private bool CheckSingle()
        {
            var single = Enumerable.Range(0, 81).Select(f => new object[] { f, table[f / 9, f % 9] }).Where(n => !((TableElement)n[1]).nums[0] && ((TableElement)n[1]).nums.Where(m => m == true).Count() == 1);
            if (single.Count() == 0) return false;
            foreach ( var item in single)
            {
                int index = (int)item[0];
                TableElement el = (TableElement)item[1];
                int value = Enumerable.Range(1, 9).Where(n => el.nums[n]).ToArray()[0];
                SetValue(index / 9, index % 9, value);
            }
            return true;
        }

        private bool LastHero()
        {
            bool Check = false;
            var rangeString = Enumerable.Range(0, 9).Select(x => new object[] { x, Enumerable.Range(1, 9).Select(f => new object[] { f, Enumerable.Range(0, 9).Where(y => table[x, y].HasVal(f)).Count() }).Where(f => ((int)f[1] == 1)).ToArray() }).Where(x => ((object[])x[1]).Count() != 0).ToArray();
            if (rangeString.Length > 0)
            {
                Check = true;
                foreach (var col in rangeString)
                {
                    int x = (int)col[0];
                    foreach (var num in (object[])col[1])
                    {
                        int value = (int)((object[])num)[0];
                        for (int y = 0; y<9; y++)
                        {
                            if (table[x,y].HasVal(value))
                            {
                                SetValue(x, y, value);
                                break;
                            }
                        }
                    }
                }
            }
            var rangeColumn = Enumerable.Range(0, 9).Select(x => new object[] { x, Enumerable.Range(1, 9).Select(f => new object[] { f, Enumerable.Range(0, 9).Where(y => table[y, x].HasVal(f)).Count() }).Where(f => ((int)f[1] == 1)).ToArray() }).Where(x => ((object[])x[1]).Count() != 0).ToArray();
            if (rangeColumn.Length > 0)
            {
                Check = true;
                foreach (var col in rangeColumn)
                {
                    int y = (int)col[0];
                    foreach (var num in (object[])col[1])
                    {
                        int value = (int)((object[])num)[0];
                        for (int x = 0; x < 9; x++)
                        {
                            if (table[x, y].HasVal(value))
                            {
                                SetValue(x, y, value);
                                break;
                            }
                        }
                    }
                }
            }
            var rangeBlock = Enumerable.Range(0, 9).Select(x => new object[] { x, Enumerable.Range(1, 9).Select(f => new object[] { f, Enumerable.Range(0, 9).Where(y => table[3*(x/3)+y/3, 3*(x%3)+y%3].HasVal(f)).Count() }).Where(f => ((int)f[1] == 1)).ToArray() }).Where(x => ((object[])x[1]).Count() != 0).ToArray();
            if (rangeBlock.Length > 0)
            {
                Check = true;
                foreach (var col in rangeBlock)
                {
                    int x = (int)col[0];
                    foreach (var num in (object[])col[1])
                    {
                        int value = (int)((object[])num)[0];
                        for (int y = 0; y < 9; y++)
                        {
                            if (table[3*(x/3)+y/3, 3*(x%3)+y%3].HasVal(value))
                            {
                                SetValue(3*(x/3)+y/3, 3*(x%3)+y%3, value);
                                break;
                            }
                        }
                    }
                }
            }
            return Check;
        }
    }
}
