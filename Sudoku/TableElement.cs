﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sudoku
{
    internal class TableElement
    {
        public bool[] nums = new bool[]{ false, true, true, true, true, true, true, true, true, true };
        public bool IsSet()
        {
            return nums[0];
        }
        public int Value()
        {
            if (nums[0])
            {
                for (int i=1; i<10; i++)
                {
                    if (nums[i]) return i;
                }
            }
            return 0;
        }
        public void Set(int value)
        {
            if (nums[0]) return;
            for (int i = 1; i < 10; i++) nums.SetValue(i == value, i);
            nums[0] = true;
        }
        public void UnSet(int value)
        {
            if (nums[0]) return;
            nums[value] = false;
            //if (nums.Where(n => n == true).Count() == 1) nums[0] = true;
        }

        public bool HasVal(int value)
        {
            return !nums[0] && nums[value];
        }
    }
}
